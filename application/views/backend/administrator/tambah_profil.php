<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><div class="page-wrapper">
	<div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title"><?php echo $title ?></h4>
            </div>
        </div>
    </div>

  <div class="page-breadcrumb">
    <div class="row">
      <div class="col-12 d-flex no-block align-items-center">
        <h4 class="page-title"><?php echo $title ?></h4>
      </div>
    </div>
  </div>

  <div class="container-fluid">

    <div class="row">
      <div class="col-md-12">
        <?php echo $this->session->flashdata('notif') ?>
        <?php echo form_open_multipart('login/simpan') ?>
        <div class="form-group">
          <label for="text">Nama Lengkap</label>
          <input type="text" name="nama" class="form-control" placeholder="Masukkan Nama Anda" required autofocus>
        </div>
        <div class="form-group">
          <label for="text">Username</label>
          <input type="text" name="username" class="form-control" placeholder="Masukkan Username" required>
        </div>
        <div class="form-group">
          <label for="text">Level</label>
          <select name="level" class="form-control">
            <option value="2">Admin</option>
            <option value="1">Master Admin</option>
          </select>
        </div>
        <div class="form-group">
          <label for="text">Password</label>
          <input type="password" name="pass" placeholder="Masukkan Password" class="form-control" required>
        </div>

        <button type="submit" class="btn btn-md btn-success">Simpan</button>
        <button type="reset" class="btn btn-md btn-warning">Reset</button>

        <?php echo form_close()
        ?>
        </form>
      </div>
    </div>
  </div>
</div>