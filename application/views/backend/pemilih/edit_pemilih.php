<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
        <!-- [ Main Content ] start -->
			<div class="row">
				<div class="col-sm-12">
					<div class="card">
						<div class="card-header">
							<h5>Edit Data User</h5>
						</div>
						<div class="card-body">
						<?php echo $this->session->flashdata('notif') ?>                
						<?php echo form_open_multipart('pemilih/update') ?>
						<div class="row">
							<div class="col-sm-12">
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<input type="hidden" name="Txtnik" value="<?php echo $data_jb->nik ?>">
								<div class="form-group">
									<label for="text">Nama Pendukung</label>
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<input type="text" name="Txtpemilih" class="form-control" placeholder="Masukkan Nama Pendukung" required value="<?php echo $data_jb->nama_pemilih; ?>">
								</div>								
								<div class="form-group">
									<label for="text">Kontak Pendukung</label>
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<input type="number" name="Txtkontak" class="form-control" placeholder="Masukkan Nomor HP Aktif" required value="<?php echo $data_jb->kontak; ?>">
								</div>								
								<div class="form-group">
									<label for="text">Status</label>
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<select class="mb-3 custom-select "  name="Txtstatus" required>
									<option disabled selected>--Pilih--</option>
									<?php
									  foreach ($jenism as $jnis) {
									?>
										<option value="<?= $jnis; ?>" <?php if (($data_jb->status) == ($jnis)) {	echo 'selected'; } ?> >
											<?= $jnis; ?>
										</option>
									<?php
									  }
									?>
									</select>
								</div>								
								<div class="form-group">
									<label for="text">TPS</label>
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<select class="mb-3 custom-select "  name="Txttps" required>
										<option disabled selected>--Pilih--</option>
										<?php
										  foreach ($jns_tps as $Dttps) {
										?>
										<option value="<?= $Dttps->id_tps ?>" <?php if (($data_jb->id_tps) == ($Dttps->id_tps)) {	echo 'selected'; } ?> >
											<?= $Dttps->nama_tps; ?>
										</option>
										
										<?php
										  }
										?>
									</select>
								</div>
							</div>
						</div>
							<button type="submit" class="btn btn-md btn-success">Update</button>
							<button type="button" class="btn btn-md btn-danger" onclick="javascript:history.back()"><span ></span> Batal</button>
							<?php echo form_close() ?>
						</div>
					</div>
				
				</div>
				<!-- [ form-element ] start -->
			</div>
		</div>
    </div>
</section>

<script type="text/javascript">
	$(document).ready(function(){		
		$('.form-checkbox').click(function(){
			if($(this).is(':checked')){
				$('.form-password').attr('type','text');
			}else{
				$('.form-password').attr('type','password');
			}
		});
	});
</script>