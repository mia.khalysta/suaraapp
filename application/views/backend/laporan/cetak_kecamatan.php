<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header("Content-type: application/octet-stream");
header("Pragma: no-cache");
header("Expires: 0");
?>
<?php
	$kecamatan=($_GET['kecamatan']);
	$query=$this->db->query('
	SELECT
	Sum(suara.paslon4) as ps4,
	Sum(suara.paslon3) as ps3,
	Sum(suara.paslon2) as ps2,
	Sum(suara.paslon1) as ps1,
	Sum(suara.paslon1+suara.paslon2+suara.paslon3+suara.paslon4) as pstot,
	suara.id_kecamatan,
	suara.id_keldes,
	count(suara.id_tps) as tps,
	Sum(suara.total_dptb) as dptb,
	Sum(dpt.total_dpt) as dpt,
	Sum(suara.tidaksah) as taksah,
	kecamatan.nama_kecamatan,
	kecamatan.dapil,
	suara.id_adm,
	keldes.nama_keldes
	FROM suara 
	INNER JOIN kecamatan ON suara.id_kecamatan = kecamatan.id_kecamatan 
	INNER JOIN keldes ON suara.id_keldes = keldes.id_keldes 
	INNER JOIN tps ON suara.id_tps = tps.id_tps
	INNER JOIN dpt ON tps.id_tps = dpt.id_tps
	WHERE 
	kecamatan.nama_kecamatan='.$kecamatan.'
	AND suara.id_kecamatan = dpt.id_kecamatan AND
	suara.id_keldes = dpt.id_keldes AND
	suara.id_tps = dpt.id_tps
	GROUP BY suara.id_keldes ASC');
	header("Content-Disposition: attachment; filename=$kecamatan.xls");
?>
<div class="col-sm-12">
	<div class="card">
		<div class="card-body">
			<table width="100%" border="0">
				<tr>
					<td width="14%"><h3><b>NAMA KECAMATAN</b></h3></td>
					<td width="76%"><h3><b>: <?php echo $kecamatan ?></b></h3></td>
				</tr>
			</table>
			<table border=1 style="width:100%;">
				<thead>
				<tr>
					<th>NO.</th>
					<th>KELURAHAN/DESA</th>
					<th>JML TPS</th>
					<th>IIN-RAHMAD</th>
					<th>JUNAIDI-SAHRANI</th>
					<th>ERYANTO-MATEUS</th>
					<th>MARTIN-FARHAN</th>
					<th>JML SUARA SAH</th>
					<th>JML TIDAK SAH</th>						
					<th>JML DPT</th>
					<th>JML DPTB</th>
					<th>DPT+DPTB</th>
					<th>SAH+TDK SAH</th>
					<th>JML GOLPUT</th>
				</tr>
				</thead>
				<tbody>
					<?php
					//untuk penomoran data
					$no=1;							
					//menampilkan data
					foreach($query->result() as $hasil){
						$dpttotall= ($hasil->dpt)+($hasil->dptb);
						$allsuara=$hasil->pstot+$hasil->taksah;
						$allgolput=$dpttotall-$allsuara;
					?>
				<tr>
					<td style="width: 3%;"><?php echo $no++ ?></td>
					<td><?php echo $hasil->nama_keldes ?></td>
					<td align="center"><?php echo $hasil->tps ?></td>
					<td align="center"><?php echo $hasil->ps1 ?></td>
					<td align="center"><?php echo $hasil->ps2 ?></td>
					<td align="center"><?php echo $hasil->ps3 ?></td>
					<td align="center"><?php echo $hasil->ps4 ?></td>
					<td align="center" class="sah"><?php echo $hasil->pstot ?></td>
					<td align="center" class="tidaksah"><?php echo $hasil->taksah ?></td>
					<td align="center" class="tidaksah"><?php echo $hasil->dpt ?></td>
					<td align="center" class="tidaksah"><?php echo $hasil->dptb ?></td>
					<td align="center" class="paslon"><?php echo $dpttotall ?></td>
					<td align="center" class="paslon"><?php echo $allsuara ?></td>
					<td align="center" class="paslon"><?php echo $allgolput ?></td>
					
				</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php
	unset($_GET['cari']);
?>