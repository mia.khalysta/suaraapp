<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- [ Main Content ] start -->
<section class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10"><?php echo $title ?></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Filter Real Count</h5>
                    </div>
					<div class="card-body">					
					<form method="POST" action="<?php echo base_url('hitungan/cetak'); ?>" class="text-center" target="_blank">
					<?php $sid = $this->session->userdata['id_pengguna'];?>	
					  <div class="row">
						<div class="col-sm-5">
							<div class="form-group">
							<input type="hidden" class="form-control" id="id_adm" name="id_adm" value="<?php echo $sid; ?>">
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<select class="custom-select"  name="keldes" id="perusahaan" required>
									<option value="" disabled selected>--Pilih Kelurahan/Desa--</option>
									<?php
										foreach ($dropdown->result() as $baris) {
										echo "<option value='".$baris->id_keldes."'>".$baris->nama_keldes."</option>";
										}
									?>
								</select>
							</div>
						</div>	
						<div class="col-sm-5">
							<div class="form-group">
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<select class="custom-select"  name="kecamatan" id="cv" readonly required>
									<option value="" disabled selected>- Tampil Kecamatan -</option>
								</select>
							</div>
						</div>
						<div class="col-sm-2">
						<button style="padding: 0.35rem 1.1875rem;" type="submit" name="cari" class="btn btn-warning  has-ripple" value="Filter">Filter</button>
						</div>
					  </div>
					</form>
					</div>
                </div>
            </div>
            
        </div>
    </div>
</section>


<script type="text/javascript">
	$(document).ready(function(){
		$('#perusahaan').on('change', function(){
			var id_perusahaan = $('#perusahaan').val();
			$.ajax({
			    type: 'POST',
			    url: '<?php echo base_url('Hitungan/tampil_chained') ?>',
			    data: { 'id' : id_perusahaan },
				success: function(data){
				    $("#cv").html(data);
				}
			})
		})
	})
</script>