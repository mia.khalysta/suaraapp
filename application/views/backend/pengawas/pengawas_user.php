<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>	
<!-- [ Main Content ] start -->
<section class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10"><?php echo $title ?></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">

            <!-- [ form-element ] start -->
            <div class="col-sm-12">
				<div class="card">
					<div class="card-header">
						<h5>Tambah Data User</h5>
					</div>
					<div class="card-body">
						<table id="example" class="stripe hover" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
							<thead>
								<tr>
									<th>No.</th>
									<th>NIK</th>
									<th>Nama</th>
									<th>Username</th>
									<th>Kecamatan</th>
									<th>Kelurahan/Desa</th>
									<th>Kontak</th>
									<th>level</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no = 1;
								foreach ($data_user as $hasil) {
									$xidu=$hasil->nik;
								?>
								<tr>
									<td style="width: 8%;"><?php echo $no++ ?></td>
									<td><?php echo $hasil->id_pengguna ?></td>
									<td><?php echo $hasil->nama_pemilih ?></td>
									<td><?php echo $hasil->username ?></td>
									<td><?php echo $hasil->nama_kecamatan ?></td>
									<td><?php echo $hasil->nama_keldes ?></td>
									<td><?php echo $hasil->kontak ?></td>
									<td><?php echo $hasil->level ?></td>
								</tr>
								<?php } ?>												
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>