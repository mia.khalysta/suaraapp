<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>	
<!-- [ Main Content ] start -->
<section class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10"><?php echo $title ?></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">

            <!-- [ form-element ] start -->
            <div class="col-sm-12">
				<div class="card">
					<div class="card-body">
						<table id="example" class="stripe hover text-center" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
								<thead>
									<tr>
										<th>No.</th>
										<th>NIK</th>
										<th>Nama Pendukung</th>
										<th>Kecamatan</th>
										<th>Kelurahan/Desa</th>
										<th>TPS</th>
										<th>Kontak</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
								<?php
								$no = 1;
								foreach ($data_pendukung as $hasil) {
								$xids=$hasil->nik;
								?>
									<tr>
										<td style="width: 8%;"><?php echo $no++ ?></td>
										<td><?php echo $hasil->nik ?></td>
										<td><?php echo $hasil->nama_pemilih ?></td>
										<td><?php echo $hasil->nama_kecamatan ?></td>
										<td><?php echo $hasil->nama_keldes ?></td>
										<td><?php foreach ($jns_tps as $stat) {
											if (($hasil->id_tps) == ($stat->id_tps)) {
											 echo $stat->nama_tps; }
										}?></td>
										<td><?php echo $hasil->kontak ?></td>
										<td><?php echo $hasil->status ?></td>
									</tr>
								<?php } ?>
								</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>