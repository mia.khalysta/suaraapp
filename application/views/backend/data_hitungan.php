<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	if ($this->session->userdata('level') == "superadmin") {
?>
<!-- [ Main Content ] start -->
<section class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10"><?php echo $title ?></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Filter Real Count</h5>
                    </div>
					<div class="card-body">
					<form method="POST" action="" class="text-center">
					<?php $sid = $this->session->userdata['id_pengguna'];?>	
					  <div class="row">
						<div class="col-sm-5">
							<div class="form-group">
							<input type="hidden" class="form-control" id="id_adm" name="id_adm" value="<?php echo $sid; ?>">
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<select class="custom-select"  name="keldes" id="perusahaan" required>
									<option value="" disabled selected>--Pilih Kelurahan/Desa--</option>
									<?php
										foreach ($dropdown->result() as $baris) {
										echo "<option value='".$baris->id_keldes."'>".$baris->nama_keldes."</option>";
										}
									?>
								</select>
							</div>
						</div>	
						<div class="col-sm-5">
							<div class="form-group">
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<select class="custom-select"  name="kecamatan" id="cv" readonly required>
									<option value="" disabled selected>- Tampil Kecamatan -</option>
								</select>
							</div>
						</div>
						<div class="col-sm-2">
						<button style="padding: 0.35rem 1.1875rem;" type="submit" name="cari" class="btn btn-warning  has-ripple" value="Filter">Filter</button>
						</div>
					  </div>
					</form>
					</div>
                </div>
            </div>
            <!-- [ form-element ] start -->
			<?php
			//di proses jika sudah klik tombol cari
			if(isset($_POST['cari'])){
				//menangkap nilai form
				$kecamatan=($_POST['kecamatan']);
				$keldes=($_POST['keldes']);
				$query=$this->db->query("SELECT suara.id_adm,suara.paslon1,suara.paslon2,suara.paslon3,suara.paslon4,suara.tidaksah,suara.total_dptb,dpt.total_dpt,kecamatan.nama_kecamatan,keldes.nama_keldes,tps.nama_tps
				FROM suara
				INNER JOIN kecamatan ON suara.id_kecamatan = kecamatan.id_kecamatan
				INNER JOIN keldes ON suara.id_keldes = keldes.id_keldes
				INNER JOIN tps ON suara.id_tps = tps.id_tps
				INNER JOIN dpt ON tps.id_tps = dpt.id_tps 
				WHERE 
				kecamatan.nama_kecamatan like '%$kecamatan%' AND keldes.id_keldes like '%$keldes%'
				AND suara.id_kecamatan = dpt.id_kecamatan AND
				suara.id_keldes = dpt.id_keldes AND
				suara.id_tps = dpt.id_tps
				ORDER BY
				kecamatan.nama_kecamatan ASC,
				keldes.nama_keldes ASC,
				tps.nama_tps ASC");
			}else{
				$query=$this->db->query("SELECT suara.id_adm,suara.paslon1,suara.paslon2,suara.paslon3,suara.paslon4,suara.tidaksah,suara.total_dptb,dpt.total_dpt,kecamatan.nama_kecamatan,keldes.nama_keldes,tps.nama_tps
				FROM suara
				INNER JOIN kecamatan ON suara.id_kecamatan = kecamatan.id_kecamatan
				INNER JOIN keldes ON suara.id_keldes = keldes.id_keldes
				INNER JOIN tps ON suara.id_tps = tps.id_tps
				INNER JOIN dpt ON tps.id_tps = dpt.id_tps
				WHERE
				suara.id_kecamatan = dpt.id_kecamatan AND
				suara.id_keldes = dpt.id_keldes AND
				suara.id_tps = dpt.id_tps
				ORDER BY
				kecamatan.nama_kecamatan ASC,
				keldes.nama_keldes ASC,
				tps.nama_tps ASC");
			}
			?>		
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
						<table id="example" class=" stripe hover text-center" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
							<thead>
							<tr>
								<th>No.</th>
								<th>Kecamatan</th>
								<th>Kel/Desa</th>
								<th>TPS</th>
								<th>Paslon 1</th>
								<th>Paslon 2</th>
								<th>Paslon 3</th>
								<th>Paslon 4</th>
								<th>Suara Sah</th>
								<th>Tidak Sah</th>
								<th>DPT+DPTB</th>
							</tr>
							</thead>
							<tbody>
								<?php
								//untuk penomoran data
								$no=1;							
								//menampilkan data
								foreach($query->result() as $hasil){
								$total= ($hasil->paslon1)+($hasil->paslon2)+($hasil->paslon3)+($hasil->paslon4);
								$dpttotall= ($hasil->total_dpt)+($hasil->total_dptb);
								?>
							<tr>
								<td style="width: 8%;"><?php echo $no++ ?></td>
								<td><?php echo $hasil->nama_kecamatan ?></td>
								<td><?php echo $hasil->nama_keldes ?></td>
								<td><?php echo $hasil->nama_tps ?></td>
								<td><?php echo $hasil->paslon1 ?></td>
								<td><?php echo $hasil->paslon2 ?></td>
								<td><?php echo $hasil->paslon3 ?></td>
								<td><?php echo $hasil->paslon4 ?></td>
								<td class="sah"><?php echo $total ?></td>
								<td class="tidaksah"><?php echo $hasil->tidaksah ?></td>
								<td class="paslon"><?php echo $dpttotall ?></td>
							</tr>
								<?php } ?>
							</tbody>
						</table>
                    </div>
                </div>
            </div>
			<?php
				unset($_POST['cari']);
			?>
        </div>
    </div>
</section>
<?php		
	}elseif($this->session->userdata('level') == "operasional") { 
?>
<section class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10"><?php echo $title ?></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Filter Real Count</h5>
                    </div>
					<div class="card-body">
					<form method="POST" action="" class="text-center">
					<?php $sid = $this->session->userdata['id_pengguna'];?>	
					  <div class="row">
						<div class="col-sm-5">
							<div class="form-group">
							<input type="hidden" class="form-control" id="id_adm" name="id_adm" value="<?php echo $sid; ?>">
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<select class="custom-select"  name="keldes" id="perusahaan" required>
									<option value="" disabled selected>- Pilih Kelurahan/Desa -</option>
									<?php
										foreach ($dropdowndapil->result() as $baris) {
										if (($this->session->userdata['id_pengguna']) == ($baris->id_pengguna)) {
										echo "<option value='".$baris->id_keldes."'>".$baris->nama_keldes."</option>";
										}}
									?>
								</select>
							</div>
						</div>	
						<div class="col-sm-5">
							<div class="form-group">
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
								<select class="form-control"  name="kecamatan" id="cv" readonly required>
									<option value="" disabled selected>- Tampil Kecamatan -</option>
								</select>
							</div>
						</div>
						<div class="col-sm-2">
						<button style="padding: 0.35rem 1.1875rem;" type="submit" name="cari" class="btn btn-warning  has-ripple" value="Filter">Filter</button>
						</div>
					  </div>
					</form>
					</div>
                </div>
            </div>
            <!-- [ form-element ] start -->
			<?php
			//di proses jika sudah klik tombol cari
			if(isset($_POST['cari'])){
				//menangkap nilai form
				$kecamatan=($_POST['kecamatan']);
				$keldes=($_POST['keldes']);
				$sids=($_POST['id_adm']);
				$query=$this->db->query("SELECT dpt.id_adm,suara.paslon1,suara.paslon2,suara.paslon3,suara.paslon4,suara.tidaksah,suara.total_dptb,dpt.total_dpt,kecamatan.nama_kecamatan,keldes.nama_keldes,tps.nama_tps
				FROM suara
				INNER JOIN kecamatan ON suara.id_kecamatan = kecamatan.id_kecamatan
				INNER JOIN keldes ON suara.id_keldes = keldes.id_keldes
				INNER JOIN tps ON suara.id_tps = tps.id_tps
				INNER JOIN dpt ON tps.id_tps = dpt.id_tps 
				WHERE 
				kecamatan.nama_kecamatan like '%$kecamatan%' AND keldes.id_keldes like '%$keldes%'
				AND suara.id_kecamatan = dpt.id_kecamatan AND
				suara.id_keldes = dpt.id_keldes AND
				suara.id_tps = dpt.id_tps
				ORDER BY
				kecamatan.nama_kecamatan ASC,
				keldes.nama_keldes ASC,
				tps.nama_tps ASC");
			}else{
				$query=$this->db->query("SELECT dpt.id_adm,suara.paslon1,suara.paslon2,suara.paslon3,suara.paslon4,suara.tidaksah,suara.total_dptb,dpt.total_dpt,kecamatan.nama_kecamatan,keldes.nama_keldes,tps.nama_tps
				FROM suara
				INNER JOIN kecamatan ON suara.id_kecamatan = kecamatan.id_kecamatan
				INNER JOIN keldes ON suara.id_keldes = keldes.id_keldes
				INNER JOIN tps ON suara.id_tps = tps.id_tps
				INNER JOIN dpt ON tps.id_tps = dpt.id_tps
				WHERE
				suara.id_kecamatan = dpt.id_kecamatan AND
				suara.id_keldes = dpt.id_keldes AND
				suara.id_tps = dpt.id_tps
				ORDER BY
				kecamatan.nama_kecamatan ASC,
				keldes.nama_keldes ASC,
				tps.nama_tps ASC");
			}
			?>		
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
						<table id="example" class=" stripe hover text-center" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
							<thead>
							<tr>
								<th>No.</th>
								<th>Kel/Desa</th>
								<th>Kecamatan</th>
								<th>TPS</th>
								<th>Paslon 1</th>
								<th>Paslon 2</th>
								<th>Paslon 3</th>
								<th>Paslon 4</th>
								<th>Suara Sah</th>
								<th>Tidak Sah</th>
								<th>DPT+DPTB</th>
							</tr>
							</thead>
							<tbody>
								<?php
								//untuk penomoran data
								$no=1;							
								//menampilkan data
								foreach($query->result() as $hasil){
								$total= ($hasil->paslon1)+($hasil->paslon2)+($hasil->paslon3)+($hasil->paslon4);
								$dpttotall= ($hasil->total_dpt)+($hasil->total_dptb);
								if (($this->session->userdata['id_pengguna']) == ($hasil->id_adm)) {
								?>
							<tr>
								<td style="width: 8%;"><?php echo $no++ ?></td>
								<td><?php echo $hasil->nama_keldes ?></td>
								<td><?php echo $hasil->nama_kecamatan ?></td>
								<td><?php echo $hasil->nama_tps ?></td>
								<td><?php echo $hasil->paslon1 ?></td>
								<td><?php echo $hasil->paslon2 ?></td>
								<td><?php echo $hasil->paslon3 ?></td>
								<td><?php echo $hasil->paslon4 ?></td>
								<td class="sah"><?php echo $total ?></td>
								<td class="tidaksah"><?php echo $hasil->tidaksah ?></td>
								<td class="paslon"><?php echo $dpttotall ?></td>
							</tr>
								<?php }} ?>
							</tbody>
						</table>
                    </div>
                </div>
            </div>
			<?php
				unset($_POST['cari']);
			?>
        </div>
    </div>
</section>


<?php
	}
?>

<script type="text/javascript">
	$(document).ready(function(){
		$('#perusahaan').on('change', function(){
			var id_perusahaan = $('#perusahaan').val();
			$.ajax({
			    type: 'POST',
			    url: '<?php echo base_url('Hitungan/tampil_chained') ?>',
			    data: { 'id' : id_perusahaan },
				success: function(data){
				    $("#cv").html(data);
				}
			})
		})
	})
</script>