<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>	
<!-- [ Main Content ] start -->
<section class="pcoded-main-container">
    <div class="pcoded-content">
		<div class="page-header">
			<div class="row">
				<div class="col-sm-12">
					<div class="card">
<?php
	if ($this->session->userdata('level') == "superadmin") {
?>					
						<div class="card-header">
							<h5></h5>
						</div>
						<div class="card-body">
							<table id="example" class="stripe hover text-center" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
								<thead>
									<tr>
										<th>No.</th>
										<th>Kecamatan</th>
										<th>Kelurahan/Desa</th>
										<th>TPS</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
								<?php
								$no = 1;
								foreach ($notif as $hasil) {
									
									?>
									<tr>
										<td style="width: 8%;"><?php echo $no++ ?></td>
										<td><?php echo $hasil->nama_kecamatan ?></td>
										<td><?php echo $hasil->nama_keldes ?></td>
										<td><?php echo $hasil->nama_tps ?></td>
										<td>
										<?php if (($hasil->status) == "Belum Mengisi") { ?>
										<div class=" btn-sm btn-danger" role="alert"><?php echo $hasil->status ?></div>
										<?php }else{ ?>
										<div class=" btn-sm btn-success" role="alert"><?php echo $hasil->status ?></div>
										<?php } ?>
										</td>
									</tr>
								<?php } ?>
								</tbody>
							</table>
						</div>
<?php		
	}elseif($this->session->userdata('level') == "operasional") {
?>
						<div class="card-header">
							<h5></h5>
						</div>
						<div class="card-body">
							<table id="example" class="stripe hover text-center" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
								<thead>
									<tr>
										<th>No.</th>
										<th>Kecamatan</th>
										<th>Kelurahan/Desa</th>
										<th>TPS</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
								<?php
								$no = 1;
								foreach ($notif_dapil as $hasil) {
									if (($this->session->userdata['id_pengguna']) == ($hasil->id_pengguna)) {
									?>
									<tr>
										<td style="width: 8%;"><?php echo $no++ ?></td>
										<td><?php echo $hasil->nama_kecamatan ?></td>
										<td><?php echo $hasil->nama_keldes ?></td>
										<td><?php echo $hasil->nama_tps ?></td>
										<td>
										<?php if (($hasil->status) == "Belum Mengisi") { ?>
										<div class=" btn-sm btn-danger" role="alert"><?php echo $hasil->status ?></div>
										<?php }else{ ?>
										<div class=" btn-sm btn-success" role="alert"><?php echo $hasil->status ?></div>
										<?php } ?>
										</td>
									</tr>
								<?php }} ?>
								</tbody>
							</table>
						</div>
<?php		
	}elseif($this->session->userdata('level') == "desa") {
?>
						<div class="card-header">
							<h5><a href="<?php echo base_url() ?>suarafile" class="btn btn-primary">FORM SUARA</a></h5>
						</div>
						<div class="card-body">
							<table id="example" class="stripe hover text-center" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
								<thead>
									<tr>
										<th>No.</th>
										<th>TPS</th>									
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
								<?php
								$no = 1;
								foreach ($notif_desa as $hasil) {
									if (($this->session->userdata['id_pengguna']) == ($hasil->id_pengguna)) {
									?>
									<tr>
										<td style="width: 8%;"><?php echo $no++ ?></td>
										<td><?php echo $hasil->nama_tps ?></td>
										<td>
										<?php if (($hasil->status) == "Belum Mengisi") { ?>
										<div class=" btn-sm btn-danger" role="alert"><?php echo $hasil->status ?></div>
										<?php }else{ ?>
										<div class=" btn-sm btn-success" role="alert"><?php echo $hasil->status ?></div>
										<?php } ?>
										</td>
									</tr>
								<?php }} ?>
								</tbody>
							</table>
						</div>
<?php
}
?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>