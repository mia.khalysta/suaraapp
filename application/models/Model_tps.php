<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_tps extends CI_model
{

  public function get_all()
  {
    $query = $this->db->select("*")
      ->from('tps')
      ->order_by('id_tps', 'ASC')
      ->get();
    return $query->result();
  }
	
  public function simpan($data)
  {
    $query = $this->db->insert("tps", $data);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function hapus($id)
  {
    $query = $this->db->delete("tps", $id);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }
  
  function get_update($xkode,$xnama){
	$query=$this->db->query("UPDATE tps SET nama_tps='$xnama' WHERE id_tps='$xkode'");
		return $query;
  }
} // END OF class Model_tps
