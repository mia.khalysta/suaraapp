<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_dapil extends CI_model
{

  public function get_all()
  {
	$query = $this->db->query("SELECT DISTINCT kecamatan.dapil FROM kecamatan, keldes WHERE kecamatan.id_kecamatan = keldes.id_kecamatan ORDER BY kecamatan.dapil ASC");
    return $query->result();
  }

} // END OF class Model_keldes
