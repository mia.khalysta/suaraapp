<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_hitungan extends CI_model
{
  
  function get_keldesk()
  {
	$tps = $this->db->select("*")
      ->from('keldes')
	  ->join('pemilih','keldes.id_keldes=pemilih.id_keldes')
      ->group_by('keldes.id_keldes', 'ASC')
      ->get();
    return $tps->result_array();
  }
  function get_tpsk()
  {
	$tps = $this->db->select("*")
      ->from('tps')
	  ->join('pemilih','tps.id_tps=pemilih.id_tps')
      ->group_by('tps.id_tps', 'ASC')
      ->get();
    return $tps->result_array();
  }
  
  function get_tps()
  {
    $this->db->order_by('id_tps','ASC');
	$tps= $this->db->get('tps');
	return $tps->result_array();
  }
  
  function provinsi(){
	$this->db->order_by('id_kecamatan','ASC');
	$provinces= $this->db->get('kecamatan');
	return $provinces->result_array();
  }

  function kabupaten($provId){
	$this->db->order_by('id_keldes','ASC');
	$kab= $this->db->get_where('keldes',array('id_kecamatan'=>$provId));
	foreach ($kab->result_array() as $data ){
	$kabupaten.= "<option value='$data[id_keldes]'>$data[nama_keldes]</option>";
	}
	return $kabupaten;
  }
  
  public function get_all()
  {
    $query = $this->db->select("*")
      ->from('suara')
      ->order_by('id_suara', 'ASC')
      ->get();
    return $query->result();
  }
  
	public function nik_tps()
	{
		$query = $this->db->select("*")
			->from('pemilih')
			->join('kecamatan','kecamatan.id_kecamatan=pemilih.id_kecamatan')
			->join('keldes','keldes.id_keldes=pemilih.id_keldes')
			->join('tps','tps.id_tps=pemilih.id_tps')
			->order_by('nik', 'ASC')
			->get();
		return $query->result();
	}
	
	public function get_nik()
	{
		$query = $this->db->select("*")
			->from('pemilih')
			->join('kecamatan','kecamatan.id_kecamatan=pemilih.id_kecamatan')
			->join('keldes','keldes.id_keldes=pemilih.id_keldes')
			->order_by('nik', 'ASC')
			->get();
		return $query->result();
	}
  
	function tampil_dropdown() //menampilkan dropdown utama
	{
		
		$query = $this->db->select("*")
		->from('keldes')
		->order_by('nama_keldes', 'ASC')
		->get();
		return $query;
	}
	
	function tampil_data_chained($id) //menampilkan dropdown kedua
	{
		$query = $this->db->query("SELECT * FROM keldes JOIN kecamatan ON keldes.id_kecamatan=kecamatan.id_kecamatan where id_keldes = '$id'");
		return $query;
	}
	
	//start per dapil
	function tampil_dropdowndapil() //menampilkan dropdown utama
	{
		$query = $this->db->select("*")
		->from('keldes')
		->join('kecamatan','kecamatan.id_kecamatan=keldes.id_kecamatan')
		->join('dapil','dapil.id_dapil=kecamatan.id_dapil')
		->join('tbl_user','tbl_user.level_dapil = dapil.id_dapil')
		->join('tbl_adm','tbl_adm.id_pengguna = tbl_user.id_pengguna')
		->order_by('keldes.nama_keldes', 'ASC')
		->get();
		return $query;
	}
	function get_d(){
	    $query = $this->db->select("*")
            ->from('suara')
			->join('kecamatan','kecamatan.id_kecamatan=suara.id_kecamatan')
			->join('keldes','keldes.id_keldes=suara.id_keldes')
			->join('dapil','dapil.id_dapil=kecamatan.id_dapil')
			->join('tbl_user','tbl_user.level_dapil = dapil.id_dapil')
			->join('tbl_adm','tbl_adm.id_pengguna = tbl_user.id_pengguna')
			->order_by('id_adm', 'ASC')
            ->get();
        return $query;
	}
	//end perdapil

    function get_j(){
	    $query = $this->db->select("*")
        ->from('suara')
		->join('kecamatan','kecamatan.id_kecamatan=suara.id_kecamatan')
		->join('keldes','keldes.id_keldes=suara.id_keldes')
		->join('tps','tps.id_tps=suara.id_tps')
        ->get();
	return $query->result();
	}

	function get_j_dapil($dapil){
		$query = $this->db->select("*")
        ->from('suara')
		->join('kecamatan a','a.id_kecamatan=suara.id_kecamatan')
		->join('keldes b','b.id_keldes=suara.id_keldes')
		->join('tps c','c.id_tps=suara.id_tps')
		->get_where('kecamatan', array('a.dapil'=>urldecode($dapil)));
	return $query->result();
	}
  
} // END OF class Model_suara
