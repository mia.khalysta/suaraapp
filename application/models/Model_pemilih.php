<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_pemilih extends CI_model
{
  public function get_all()
  {
    $query = $this->db->select("*")
      ->from('pemilih')
      ->order_by('nik', 'ASC')
      ->get();
    return $query->result();
  }
  public function get_table()
  {
    $query = $this->db->select("*")
      ->from('pemilih')
      ->order_by('nik', 'ASC')
      ->get();
    return $query->result();
  }
  
    function tampil_dropdown() //menampilkan dropdown utama
	{
		
		$query = $this->db->select("*")
		->from('keldes')
		->order_by('nama_keldes', 'ASC')
		->get();
		return $query;
	}
	
	function tampil_data_chained($id) //menampilkan dropdown kedua
	{
		$query = $this->db->query("SELECT * FROM keldes JOIN kecamatan ON keldes.id_kecamatan=kecamatan.id_kecamatan where id_keldes = '$id'");
		return $query;
	}
	function tampil_data_chained2($id) //menampilkan dropdown kedua
	{
		$query = $this->db->query("SELECT * FROM tps JOIN dpt ON tps.id_tps=dpt.id_tps JOIN keldes ON keldes.id_keldes=dpt.id_keldes JOIN kecamatan ON keldes.id_kecamatan=kecamatan.id_kecamatan where keldes.id_keldes = '$id'");
		return $query;
	}
	
	//start per dapil
	function tampil_dropdowndapil() //menampilkan dropdown utama
	{
		$query = $this->db->select("*")
		->from('keldes')
		->join('kecamatan','kecamatan.id_kecamatan=keldes.id_kecamatan')
		->join('dapil','dapil.id_dapil=kecamatan.id_dapil')
		->join('tbl_user','tbl_user.level_dapil = dapil.id_dapil')
		->join('tbl_adm','tbl_adm.id_pengguna = tbl_user.id_pengguna')
		->order_by('keldes.nama_keldes', 'ASC')
		->get();
		return $query;
	}
	function get_d(){
	    $query = $this->db->select("*")
            ->from('pemilih')
			->join('kecamatan','kecamatan.id_kecamatan=pemilih.id_kecamatan')
			->join('keldes','keldes.id_keldes=pemilih.id_keldes')
			->join('dapil','dapil.id_dapil=kecamatan.id_dapil')
			->join('tbl_user','tbl_user.level_dapil = dapil.id_dapil')
			->join('tbl_adm','tbl_adm.id_pengguna = tbl_user.id_pengguna')
			->order_by('nik', 'ASC')
            ->get();
        return $query->result();
	}
	//end perdapil
	
	function get_j(){
	    $query = $this->db->select("*")
            ->from('pemilih')
			->join('kecamatan','kecamatan.id_kecamatan=pemilih.id_kecamatan')
			->join('keldes','keldes.id_keldes=pemilih.id_keldes')
			->order_by('nik', 'ASC')
            ->get();
        return $query->result();
	}
	
  public function get_kecamatan()
  {
    $query = $this->db->select("*")
      ->from('kecamatan')
      ->order_by('id_kecamatan', 'ASC')
      ->get();
    return $query->result();
  }
  public function get_keldes()
  {
    $query = $this->db->select("*")
      ->from('keldes')
      ->order_by('id_keldes', 'ASC')
      ->get();
    return $query->result();
  }
  public function get_tps()
  {
    $query = $this->db->select("*")
      ->from('tps')
      ->order_by('id_tps', 'ASC')
      ->get();
    return $query->result();
  }
  
  public function simpan($data)
  {
    $query = $this->db->insert("pemilih", $data);
    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function edit($idjb)
  {
    $query = $this->db->where("nik", $idjb)
      ->get("pemilih");
    if ($query) {
      return $query->row();
    } else {
      return false;
    }
  }

  public function update($data, $id)
  {
    $query = $this->db->update("pemilih", $data, $id);
    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function hapus($id)
  {
    $query = $this->db->delete("pemilih", $id);
    if ($query) {
      return true;
    } else {
      return false;
    }
  }
  
  function get_update($xkode,$nama_pendukung,$kontak,$tps,$status){
	$query=$this->db->query("UPDATE pemilih SET nama_pemilih='$nama_pendukung',kontak='$kontak',id_tps='$tps',status='$status' WHERE nik='$xkode'");
		return $query;
  }
} // END OF class Model_pemilih