<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_notify extends CI_model
{

  public function tps_desa()
  {
    $query = $this->db->query("SELECT
	*
	FROM
	keldes
	JOIN kecamatan ON keldes.id_kecamatan = kecamatan.id_kecamatan
	JOIN pemilih ON keldes.id_keldes = pemilih.id_keldes
	JOIN tbl_adm ON pemilih.nik = tbl_adm.id_pengguna
	JOIN notifikasi ON notifikasi.id_keldes = pemilih.id_keldes
	JOIN tps ON tps.id_tps= notifikasi.id_tps
	ORDER BY
	tps.nama_tps ASC,
	keldes.nama_keldes ASC,
	kecamatan.nama_kecamatan ASC
	");
    return $query->result();
  }
  
  public function get_dapil()
  {
    $query = $this->db->select("*")
		->from('notifikasi')
		->join('keldes','keldes.id_keldes=notifikasi.id_keldes')
		->join('tps','tps.id_tps=notifikasi.id_tps')
		->join('kecamatan','kecamatan.id_kecamatan=keldes.id_kecamatan')
		->join('dapil','dapil.id_dapil=kecamatan.id_dapil')
		->join('tbl_user','tbl_user.level_dapil = dapil.id_dapil')
		->join('tbl_adm','tbl_adm.id_pengguna = tbl_user.id_pengguna')
		->order_by('nama_kecamatan', 'ASC')
		->order_by('nama_keldes', 'ASC')
		->order_by('nama_tps', 'ASC')
		->get();
    return $query->result();
  }
  
  public function get_all()
  {
    $query = $this->db->select("*")
		->from('notifikasi')
		->join('kecamatan','kecamatan.id_kecamatan=notifikasi.id_kecamatan')
		->join('keldes','keldes.id_keldes=notifikasi.id_keldes')
		->join('tps','tps.id_tps=notifikasi.id_tps')
		->order_by('nama_kecamatan', 'ASC')
		->order_by('nama_keldes', 'ASC')
		->order_by('nama_tps', 'ASC')
      ->get();
    return $query->result();
  }
} // END OF class Model_notif
