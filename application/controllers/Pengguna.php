<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('model_pengguna');
		$this->load->library('form_validation');
        if ($this->session->userdata('status') != "loginCOD") {
            redirect(base_url("log-in"));
        }
    }

    function index()
    {
        $this->load->view('index');
    }

    function datapengguna()
    {
        $data = array(
            'title' => 'Data Pengguna',
            'data_allpengguna' => $this->model_pengguna->get_all(),
			'data_pengguna' => $this->model_pengguna->get_op(),
			'jns_nik' => $this->model_pengguna->get_nik(),
			'jns_dapil' => $this->model_pengguna->get_dapil(),
            'isi' => 'backend/pengguna/data_penggunafull'
        );
        $this->load->view('backend/layout/wrapper', $data);
    }

	public function insert()
	{
		$data = array(
			'id_pengguna' => htmlspecialchars($this->input->post('Txtnik', true)),
			'level' => htmlspecialchars($this->input->post('Txtlevel', true)),
            'username' => htmlspecialchars($this->input->post('username', true)),
            'password' => md5($this->input->post('pass'))
        );
		
		$data2 = array(
			'id_pengguna' => htmlspecialchars($this->input->post('Txtnik', true)),
			'level_dapil' => htmlspecialchars($this->input->post('Txtleveldapil', true)),
        );
		
		$nik = $this->input->post('Txtnik');
		$level = $this->input->post('Txtlevel');
		$sql = $this->db->query("SELECT id_pengguna FROM tbl_adm where id_pengguna='$nik' AND level='$level' ");
		$cek_nik = $sql->num_rows();
		if ($cek_nik > 0) {
		$this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4>Peringatan! Data Sudah Ada</h4></div>');
		redirect('userfile');
		}else{
		$this->db->insert('tbl_adm',$data);
		$this->db->insert('tbl_user',$data2);
		$this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4>Success! Data Berhasil di Simpan<h4></div>');
		redirect('userfile');
		}
		
	}
	
	public function insert2()
	{
		$data2 = array(
			'id_pengguna' => htmlspecialchars($this->input->post('Txtnik', true)),
			'level' => htmlspecialchars($this->input->post('Txtlevel', true)),
            'username' => htmlspecialchars($this->input->post('username', true)),
            'password' => md5($this->input->post('pass'))
        );
		
		$nik = $this->input->post('Txtnik');
		$sql = $this->db->query("SELECT id_pengguna FROM tbl_adm where id_pengguna='$nik'");
		$cek_nik = $sql->num_rows();
		if ($cek_nik > 0) {
		$this->session->set_flashdata('notif', '<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Peringatan! Data Sudah Ada</div>');
		redirect('userfile');
		}else{
		$this->db->insert('tbl_adm',$data2);
		$this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data Pembelian berhasil di simpan</div>');
		redirect('userfile');
		}
		
	}
	
	public function edit($idp)
    {
        $data = array(
            'title' => 'Edit Profil',
			'data_u' => $this->model_pengguna->edit_user($idp),
            'data_p' => $this->model_pengguna->edit($idp),
			'data_x' => $this->model_pengguna->editx($idp),
			'jns_nik' => $this->model_pengguna->get_nik(),
			'jns_dapil' => $this->model_pengguna->get_dapil(),
			'jenism' => ['desa', 'tps'],
			
            'isi' => 'backend/pengguna/edit_pengguna'
        );
        $this->load->view('backend/layout/wrapper', $data);
    }
	
    public function update()
    {
		$id_u['id_user'] = $this->input->post("idu");
        $password = $this->input->post("pass");
		
        $data = array(
			'id_pengguna' => htmlspecialchars($this->input->post('nik', true)),
			'level' => htmlspecialchars($this->input->post('status', true)),
            'username' => htmlspecialchars($this->input->post('username', true)),
            'password' => md5($password)
        );
		$this->model_pengguna->update_user($data, $id_u);
		
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data Pengguna berhasil di Update</div>');

        redirect('userfile');
    }
	
	public function hapus($id)
    {
        $idu['id_pengguna'] = $this->uri->segment(3);

        $this->model_pengguna->hapusu($idu);
        $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! data berhasil di hapus</div>');
        redirect('userfile');
    }
}
