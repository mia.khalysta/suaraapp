<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suara extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_suara');
    $this->load->helper(array('form', 'url'));
	$this->load->library('form_validation');
        if ($this->session->userdata('status') != "loginCOD") {
            redirect(base_url("log-in"));
        }
  }

  public function index()
  {

    $this->load->view('index');
  }

  public function datasuara()
  {
    $data = array(
      'title' => 'Data Suara',
      'data_suara' => $this->model_suara->get_j(),
	  'dropdown' => $this->model_suara->tampil_dropdown(),
	  
	  'data_perdapil' => $this->model_suara->get_d(),
	  'dropdowndapil' => $this->model_suara->tampil_dropdowndapil(),
	  'jns_tpsdesa' => $this->model_suara->get_tpsdesa(),
	  
	  'dropdown_dapil' => $this->model_suara->tampil_dapil(),
	  'nik' => $this->model_suara->get_nik(),
	  'nikpl' => $this->model_suara->nik_tps(),
	  'jns_keldes' => $this->model_suara->get_keldes(),
	  'jns_tps' => $this->model_suara->get_tps(),
	  'jns_tpsd' => $this->model_suara->get_tpsd(),
      'isi' => 'backend/suara/data_suara'
    );
		$this->form_validation->set_rules('id_adm', 'id_adm', 'required|numeric|trim');
		$this->form_validation->set_rules('Txtpaslon1', 'Paslon 1', 'required|numeric|trim');
		$this->form_validation->set_rules('Txtpaslon2', 'Paslon 2', 'required|numeric|trim');
		$this->form_validation->set_rules('Txtpaslon3', 'Paslon 3', 'required|numeric|trim');
		$this->form_validation->set_rules('Txtpaslon4', 'Paslon 4', 'required|numeric|trim');
		$this->form_validation->set_rules('Txtkecamatan', 'Kecamatan', 'required');
        $this->form_validation->set_rules('Txtkeldes', 'Kelurahan/Desa', 'required');
		$this->form_validation->set_rules('Txttps', 'TPS', 'required');
		$this->form_validation->set_rules('Txttidaksah', 'Tidak Sah', 'required|numeric|trim');
		$this->form_validation->set_rules('Txtdptb', 'Total DPTB', 'required|numeric|trim');

        if ($this->form_validation->run() == false) {
            //GAGAL
            $this->load->view('backend/layout/wrapper', $data);
        } else {
            //BERHASIL
            $this->simpan();
        }
  }
	
	function tampil_chained()
	{
		$id = $_POST['id'];
		$dropdown_chained = $this->model_suara->tampil_data_chained($id);
		foreach ($dropdown_chained->result() as $baris) {
			echo "<option value='".$baris->id_kecamatan."'>".$baris->nama_kecamatan."</option>";
		}
	}
	function tampil_chained2()
	{
		$id = $_POST['id'];
		$dropdown_chained = $this->model_pemilih->tampil_data_chained2($id);
		foreach ($dropdown_chained->result() as $baris) { //masukan fild id dan nama dari dropdown secondarynya
			echo "<option value='".$baris->id_tps."'>".$baris->nama_tps."</option>";
		}
	}
	
  public function simpan()
  {
    $data = array(
	  'id_adm' => htmlspecialchars($this->input->post("id_adm", true)),
	  'paslon1' => htmlspecialchars($this->input->post("Txtpaslon1", true)),
      'paslon2' => htmlspecialchars($this->input->post("Txtpaslon2", true)),
	  'paslon3' => htmlspecialchars($this->input->post("Txtpaslon3", true)),
	  'paslon4' => htmlspecialchars($this->input->post("Txtpaslon4", true)),
	  'id_kecamatan' => htmlspecialchars($this->input->post("Txtkecamatan", true)),
	  'id_keldes' => htmlspecialchars($this->input->post("Txtkeldes", true)),
	  'id_tps' => htmlspecialchars($this->input->post("Txttps", true)),
	  'tidaksah' => htmlspecialchars($this->input->post("Txttidaksah", true)),
	  'total_dptb' => htmlspecialchars($this->input->post("Txtdptb", true))
    );
	//notifikasi
	$kec['id_kecamatan'] = htmlspecialchars($this->input->post("Txtkecamatan", true));
	$keldes['id_keldes'] = htmlspecialchars($this->input->post("Txtkeldes", true));
	$tps['id_tps'] = htmlspecialchars($this->input->post("Txttps", true));
	$data2 = array(
	  'user_update' => htmlspecialchars($this->input->post("id_adm", true)),
	  'tanggal_update' => htmlspecialchars(date("Y-m-d H:i:s"), true),
	  'status' => "Sudah Mengisi"
    );
	
	$kecamatan = $this->input->post('Txtkecamatan');
	$keldes = $this->input->post('Txtkeldes');
	$tps = $this->input->post('Txttps');
	$sql = $this->db->query("SELECT id_kecamatan,id_keldes,id_tps FROM suara where id_kecamatan='$kecamatan' AND id_keldes='$keldes' AND id_tps='$tps'");
	$cek_nik = $sql->num_rows();
	if ($cek_nik > 0) {
	$this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4>Peringatan! Data Sudah Ada</h4></div>');
	redirect('suarafile');
	}else{
	//notifikasi
	$this->db->update('notifikasi',$data2,array(
	'id_kecamatan' => htmlspecialchars($this->input->post("Txtkecamatan", true)),
	'id_keldes' => htmlspecialchars($this->input->post("Txtkeldes", true)),
	'id_tps' => htmlspecialchars($this->input->post("Txttps", true)))
	);
	
    $this->model_suara->simpan($data);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4>Success! Data Berhasil di Simpan<h4></div>');
    redirect('suarafile');
	}
	
    
  }

  public function edit($idjb)
  {
    $idjb = $this->uri->segment(3);

    $data = array(
      'title'     => 'Edit Suara',
      'data_suara' => $this->model_suara->edit($idjb),
      'isi' => 'backend/suara/edit_suara'
    );

    $this->load->view('backend/layout/wrapper', $data);
  }

  public function update()
  {
    $id_s['id_suara'] = $this->input->post("Txtsuara");
	
    $data = array(
	  'tidaksah' 	=> htmlspecialchars($this->input->post("Txttidaksah", true)),
	  'total_dptb' 	=> htmlspecialchars($this->input->post("Txtdptb", true)),
      'paslon1' 	=> htmlspecialchars($this->input->post("Txtpaslon1", true)),
      'paslon2' 	=> htmlspecialchars($this->input->post("Txtpaslon2", true)),
	  'paslon3' 	=> htmlspecialchars($this->input->post("Txtpaslon3", true)),
	  'paslon4' 	=> htmlspecialchars($this->input->post("Txtpaslon4", true))
    );

    $this->model_suara->update($data, $id_s);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data Berhasil di Update</div>');

    redirect('suarafile');
  }

  public function hapus($idj)
  {
    $id['id_suara'] = $this->uri->segment(3);
    $this->model_suara->hapus($id);
    redirect('suarafile');
  }

} // END OF class Suara
