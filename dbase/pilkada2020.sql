/*
Navicat MySQL Data Transfer

Source Server         : Aisyah
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : pilkada2020

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-11-22 14:17:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `dapil`
-- ----------------------------
DROP TABLE IF EXISTS `dapil`;
CREATE TABLE `dapil` (
  `id_dapil` int(11) NOT NULL AUTO_INCREMENT,
  `nama_dapil` varchar(11) NOT NULL,
  PRIMARY KEY (`id_dapil`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of dapil
-- ----------------------------
INSERT INTO `dapil` VALUES ('1', 'Dapil 1');
INSERT INTO `dapil` VALUES ('2', 'Dapil 2');
INSERT INTO `dapil` VALUES ('3', 'Dapil 3');
INSERT INTO `dapil` VALUES ('4', 'Dapil 4');
INSERT INTO `dapil` VALUES ('5', 'Dapil 5');
INSERT INTO `dapil` VALUES ('6', 'Dapil 6');

-- ----------------------------
-- Table structure for `dpt`
-- ----------------------------
DROP TABLE IF EXISTS `dpt`;
CREATE TABLE `dpt` (
  `id_dpt` int(11) NOT NULL AUTO_INCREMENT,
  `id_adm` varchar(16) NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `id_keldes` int(11) NOT NULL,
  `id_tps` int(11) NOT NULL,
  `total_dpt` int(11) NOT NULL,
  `tanggal` date NOT NULL DEFAULT current_timestamp(),
  `waktu` time NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_dpt`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of dpt
-- ----------------------------
INSERT INTO `dpt` VALUES ('73', '1001', '3', '23', '1', '150', '2020-11-21', '18:13:25');
INSERT INTO `dpt` VALUES ('74', '1001', '2', '15', '2', '200', '2020-11-21', '18:13:41');

-- ----------------------------
-- Table structure for `kecamatan`
-- ----------------------------
DROP TABLE IF EXISTS `kecamatan`;
CREATE TABLE `kecamatan` (
  `id_kecamatan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kecamatan` varchar(40) NOT NULL,
  `id_dapil` int(11) NOT NULL,
  `dapil` varchar(11) NOT NULL,
  PRIMARY KEY (`id_kecamatan`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of kecamatan
-- ----------------------------
INSERT INTO `kecamatan` VALUES ('1', 'AIR UPAS', '5', 'Dapil 5');
INSERT INTO `kecamatan` VALUES ('2', 'BENUA KAYONG', '6', 'Dapil 6');
INSERT INTO `kecamatan` VALUES ('3', 'DELTA PAWAN', '1', 'Dapil 1');
INSERT INTO `kecamatan` VALUES ('4', 'HULU SUNGAI', '3', 'Dapil 3');
INSERT INTO `kecamatan` VALUES ('5', 'JELAI HULU', '4', 'Dapil 4');
INSERT INTO `kecamatan` VALUES ('6', 'KENDAWANGAN', '6', 'Dapil 6');
INSERT INTO `kecamatan` VALUES ('7', 'MANIS MATA', '5', 'Dapil 5');
INSERT INTO `kecamatan` VALUES ('8', 'MARAU', '5', 'Dapil 5');
INSERT INTO `kecamatan` VALUES ('9', 'MATAN HILIR SELATAN', '6', 'Dapil 6');
INSERT INTO `kecamatan` VALUES ('10', 'MATAN HILIR UTARA', '1', 'Dapil 1');
INSERT INTO `kecamatan` VALUES ('11', 'MUARA PAWAN', '1', 'Dapil 1');
INSERT INTO `kecamatan` VALUES ('12', 'NANGA TAWAP', '3', 'Dapil 3');
INSERT INTO `kecamatan` VALUES ('13', 'PEMAHAN', '4', 'Dapil 4');
INSERT INTO `kecamatan` VALUES ('14', 'SANDAI', '3', 'Dapil 3');
INSERT INTO `kecamatan` VALUES ('15', 'SIMPANG DUA', '2', 'Dapil 2');
INSERT INTO `kecamatan` VALUES ('16', 'SIMPANG HULU', '2', 'Dapil 2');
INSERT INTO `kecamatan` VALUES ('17', 'SINGKUP', '5', 'Dapil 5');
INSERT INTO `kecamatan` VALUES ('18', 'SUNGAI LAUR', '2', 'Dapil 2');
INSERT INTO `kecamatan` VALUES ('19', 'SUNGAI MELAYU RAYAK', '4', 'Dapil 4');
INSERT INTO `kecamatan` VALUES ('20', 'TUMBANG TITI', '4', 'Dapil 4');

-- ----------------------------
-- Table structure for `keldes`
-- ----------------------------
DROP TABLE IF EXISTS `keldes`;
CREATE TABLE `keldes` (
  `id_keldes` int(11) NOT NULL AUTO_INCREMENT,
  `nama_keldes` varchar(100) DEFAULT NULL,
  `id_kecamatan` int(11) DEFAULT NULL,
  `jml_tps` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_keldes`)
) ENGINE=InnoDB AUTO_INCREMENT=263 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of keldes
-- ----------------------------
INSERT INTO `keldes` VALUES ('1', 'AIR DURIAN JAYA', '1', '2');
INSERT INTO `keldes` VALUES ('2', 'AIR UPAS', '1', '17');
INSERT INTO `keldes` VALUES ('3', 'BANDA SARI', '1', '2');
INSERT INTO `keldes` VALUES ('4', 'GAHANG', '1', '4');
INSERT INTO `keldes` VALUES ('5', 'HARAPAN BARU', '1', '9');
INSERT INTO `keldes` VALUES ('6', 'MEKAR JAYA', '1', '2');
INSERT INTO `keldes` VALUES ('7', 'MEMBULUH BARU', '1', '4');
INSERT INTO `keldes` VALUES ('8', 'SARI BEKAYAS', '1', '2');
INSERT INTO `keldes` VALUES ('9', 'SUKA RIA', '1', '3');
INSERT INTO `keldes` VALUES ('10', 'BANJAR', '2', '3');
INSERT INTO `keldes` VALUES ('11', 'BARU', '2', '8');
INSERT INTO `keldes` VALUES ('12', 'KAUMAN', '2', '12');
INSERT INTO `keldes` VALUES ('13', 'KINJIL PESISIR', '2', '5');
INSERT INTO `keldes` VALUES ('14', 'MEKAR SARI', '2', '6');
INSERT INTO `keldes` VALUES ('15', 'MULIA KERTA', '2', '16');
INSERT INTO `keldes` VALUES ('16', 'NEGERI BARU', '2', '6');
INSERT INTO `keldes` VALUES ('17', 'PADANG', '2', '8');
INSERT INTO `keldes` VALUES ('18', 'SUKA BARU', '2', '5');
INSERT INTO `keldes` VALUES ('19', 'SUNGAI KINJIL', '2', '5');
INSERT INTO `keldes` VALUES ('20', 'TUAN-TUAN', '2', '10');
INSERT INTO `keldes` VALUES ('21', 'KALI NILAM', '3', '19');
INSERT INTO `keldes` VALUES ('22', 'KANTOR', '3', '14');
INSERT INTO `keldes` VALUES ('23', 'MULIA BARU', '3', '26');
INSERT INTO `keldes` VALUES ('24', 'PAYAK KUMANG', '3', '13');
INSERT INTO `keldes` VALUES ('25', 'SAMPIT', '3', '32');
INSERT INTO `keldes` VALUES ('26', 'SUKA BANGUN', '3', '14');
INSERT INTO `keldes` VALUES ('27', 'SUKA HARJA', '3', '30');
INSERT INTO `keldes` VALUES ('28', 'SUKABANGUN DALAM', '3', '7');
INSERT INTO `keldes` VALUES ('29', 'TENGAH', '3', '18');
INSERT INTO `keldes` VALUES ('30', 'BATU LAPIS', '4', '2');
INSERT INTO `keldes` VALUES ('31', 'BEGINCI DARAT', '4', '1');
INSERT INTO `keldes` VALUES ('32', 'BENUA KRIO', '4', '3');
INSERT INTO `keldes` VALUES ('33', 'CINTA MANIS', '4', '2');
INSERT INTO `keldes` VALUES ('34', 'KENYABUR', '4', '2');
INSERT INTO `keldes` VALUES ('35', 'KRIO HULU', '4', '3');
INSERT INTO `keldes` VALUES ('36', 'LUBUK KAKAP', '4', '1');
INSERT INTO `keldes` VALUES ('37', 'MENYUMBUNG', '4', '5');
INSERT INTO `keldes` VALUES ('38', 'RIAM DADAP', '4', '1');
INSERT INTO `keldes` VALUES ('39', 'SEKUKUN', '4', '2');
INSERT INTO `keldes` VALUES ('40', 'SENDURUHAN', '4', '3');
INSERT INTO `keldes` VALUES ('41', 'SUNGE BENGARAS', '4', '3');
INSERT INTO `keldes` VALUES ('42', 'AIR DUA', '5', '2');
INSERT INTO `keldes` VALUES ('43', 'ASAM JELAI', '5', '2');
INSERT INTO `keldes` VALUES ('44', 'BAYAM RAYA', '5', '1');
INSERT INTO `keldes` VALUES ('45', 'BIKU SARANA', '5', '2');
INSERT INTO `keldes` VALUES ('46', 'DERANUK', '5', '4');
INSERT INTO `keldes` VALUES ('47', 'KARANG DANGIN', '5', '1');
INSERT INTO `keldes` VALUES ('48', 'KESUMA JAYA', '5', '2');
INSERT INTO `keldes` VALUES ('49', 'KUSIK BATU LAPU', '5', '2');
INSERT INTO `keldes` VALUES ('50', 'LIMPANG', '5', '2');
INSERT INTO `keldes` VALUES ('51', 'PANGKALAN PAKET', '5', '1');
INSERT INTO `keldes` VALUES ('52', 'PANGKALAN SUKA', '5', '3');
INSERT INTO `keldes` VALUES ('53', 'PASIR MAYANG', '5', '2');
INSERT INTO `keldes` VALUES ('54', 'PENYARANG', '5', '2');
INSERT INTO `keldes` VALUES ('55', 'PERIANGAN', '5', '4');
INSERT INTO `keldes` VALUES ('56', 'PERIGI', '5', '2');
INSERT INTO `keldes` VALUES ('57', 'RANGGA INTAN', '5', '1');
INSERT INTO `keldes` VALUES ('58', 'RIAM DANAU KANAN', '5', '3');
INSERT INTO `keldes` VALUES ('59', 'SEMANTUN', '5', '3');
INSERT INTO `keldes` VALUES ('60', 'SIDAHARI', '5', '1');
INSERT INTO `keldes` VALUES ('61', 'TANGGERANG', '5', '3');
INSERT INTO `keldes` VALUES ('62', 'TEBING BERSERI', '5', '2');
INSERT INTO `keldes` VALUES ('63', 'TELUK RUNJAI', '5', '2');
INSERT INTO `keldes` VALUES ('64', 'AIR HITAM BESAR', '6', '6');
INSERT INTO `keldes` VALUES ('65', 'AIR TARAP', '6', '3');
INSERT INTO `keldes` VALUES ('66', 'AIRHITAM HULU', '6', '5');
INSERT INTO `keldes` VALUES ('67', 'BANGKAL SERAI', '6', '5');
INSERT INTO `keldes` VALUES ('68', 'BANJAR SARI', '6', '9');
INSERT INTO `keldes` VALUES ('69', 'DANAU BUNTAR', '6', '6');
INSERT INTO `keldes` VALUES ('70', 'KEDONDONG', '6', '3');
INSERT INTO `keldes` VALUES ('71', 'KENDAWANGAN KANAN', '6', '3');
INSERT INTO `keldes` VALUES ('72', 'KENDAWANGAN KIRI', '6', '18');
INSERT INTO `keldes` VALUES ('73', 'KERAMAT JAYA', '6', '3');
INSERT INTO `keldes` VALUES ('74', 'MEKAR UTAMA', '6', '13');
INSERT INTO `keldes` VALUES ('75', 'NATAI KUINI', '6', '2');
INSERT INTO `keldes` VALUES ('76', 'PANGKALAN BATU', '6', '4');
INSERT INTO `keldes` VALUES ('77', 'PEMBEDILAN', '6', '5');
INSERT INTO `keldes` VALUES ('78', 'SELIMANTAN JAYA', '6', '4');
INSERT INTO `keldes` VALUES ('79', 'SERIAM', '6', '4');
INSERT INTO `keldes` VALUES ('80', 'SUKA DAMAI', '6', '2');
INSERT INTO `keldes` VALUES ('81', 'SUKA HARAPAN', '6', '2');
INSERT INTO `keldes` VALUES ('82', 'SUNGAI JELAYAN', '6', '3');
INSERT INTO `keldes` VALUES ('83', 'AIR DEKAKAH', '7', '3');
INSERT INTO `keldes` VALUES ('84', 'ASAM BESAR', '7', '7');
INSERT INTO `keldes` VALUES ('85', 'BATU SEDAU', '7', '3');
INSERT INTO `keldes` VALUES ('86', 'BUKIT GAJAH', '7', '2');
INSERT INTO `keldes` VALUES ('87', 'JAMBI', '7', '2');
INSERT INTO `keldes` VALUES ('88', 'KELAMPAI', '7', '2');
INSERT INTO `keldes` VALUES ('89', 'KELIMANTAN', '7', '2');
INSERT INTO `keldes` VALUES ('90', 'KEMUNING', '7', '2');
INSERT INTO `keldes` VALUES ('91', 'LEMBAH MUKTI', '7', '2');
INSERT INTO `keldes` VALUES ('92', 'MANIS MATA', '7', '8');
INSERT INTO `keldes` VALUES ('93', 'MEKAR JAYA', '7', '2');
INSERT INTO `keldes` VALUES ('94', 'PAKIT SELABA', '7', '3');
INSERT INTO `keldes` VALUES ('95', 'PELEMPANGAN', '7', '3');
INSERT INTO `keldes` VALUES ('96', 'RATU ELOK', '7', '6');
INSERT INTO `keldes` VALUES ('97', 'SEGULING', '7', '4');
INSERT INTO `keldes` VALUES ('98', 'SENGKUANG MERABONG', '7', '2');
INSERT INTO `keldes` VALUES ('99', 'SILAT', '7', '5');
INSERT INTO `keldes` VALUES ('100', 'SUAK BURUNG', '7', '4');
INSERT INTO `keldes` VALUES ('101', 'SUKA RAMAI', '7', '2');
INSERT INTO `keldes` VALUES ('102', 'SUNGAI BULUH', '7', '2');
INSERT INTO `keldes` VALUES ('103', 'TERUSAN', '7', '5');
INSERT INTO `keldes` VALUES ('104', 'TRIBUN JAYA', '7', '1');
INSERT INTO `keldes` VALUES ('105', 'BANTAN SARI', '8', '4');
INSERT INTO `keldes` VALUES ('106', 'BATU PAYUNG DUA', '8', '4');
INSERT INTO `keldes` VALUES ('107', 'BELABAN', '8', '6');
INSERT INTO `keldes` VALUES ('108', 'KARYA BARU', '8', '4');
INSERT INTO `keldes` VALUES ('109', 'PELANJAU JAYA', '8', '2');
INSERT INTO `keldes` VALUES ('110', 'RANDAI', '8', '3');
INSERT INTO `keldes` VALUES ('111', 'RANGKUNG', '8', '2');
INSERT INTO `keldes` VALUES ('112', 'RIAM BATU GADING', '8', '4');
INSERT INTO `keldes` VALUES ('113', 'RUNJAI JAYA', '8', '2');
INSERT INTO `keldes` VALUES ('114', 'SUKA KARYA', '8', '6');
INSERT INTO `keldes` VALUES ('115', 'HARAPAN BARU', '9', '5');
INSERT INTO `keldes` VALUES ('116', 'KEMUNING BIUTAK', '9', '3');
INSERT INTO `keldes` VALUES ('117', 'PAGAR MENTIMUN', '9', '2');
INSERT INTO `keldes` VALUES ('118', 'PEMATANG GADUNG', '9', '6');
INSERT INTO `keldes` VALUES ('119', 'PESAGUAN KANAN', '9', '10');
INSERT INTO `keldes` VALUES ('120', 'PESAGUAN KIRI', '9', '8');
INSERT INTO `keldes` VALUES ('121', 'SUNGAI BAKAU', '9', '6');
INSERT INTO `keldes` VALUES ('122', 'SUNGAI BESAR', '9', '9');
INSERT INTO `keldes` VALUES ('123', 'SUNGAI JAWI', '9', '6');
INSERT INTO `keldes` VALUES ('124', 'SUNGAI NANJUNG', '9', '7');
INSERT INTO `keldes` VALUES ('125', 'SUNGAI PELANG', '9', '11');
INSERT INTO `keldes` VALUES ('126', 'KUALA SATONG', '10', '7');
INSERT INTO `keldes` VALUES ('127', 'KUALA TOLAK', '10', '9');
INSERT INTO `keldes` VALUES ('128', 'LAMAN SATONG', '10', '8');
INSERT INTO `keldes` VALUES ('129', 'SUNGAI PUTRI', '10', '6');
INSERT INTO `keldes` VALUES ('130', 'TANJUNG BAIK BUDI', '10', '8');
INSERT INTO `keldes` VALUES ('131', 'MAYAK', '11', '2');
INSERT INTO `keldes` VALUES ('132', 'SEI AWAN KANAN', '11', '7');
INSERT INTO `keldes` VALUES ('133', 'SEI AWAN KIRI', '11', '8');
INSERT INTO `keldes` VALUES ('134', 'SUKA MAJU', '11', '4');
INSERT INTO `keldes` VALUES ('135', 'TANJUNG PASAR', '11', '2');
INSERT INTO `keldes` VALUES ('136', 'TANJUNG PURA', '11', '2');
INSERT INTO `keldes` VALUES ('137', 'TEMPURUKAN', '11', '4');
INSERT INTO `keldes` VALUES ('138', 'ULAK MEDANG', '11', '2');
INSERT INTO `keldes` VALUES ('139', 'BATU MAS', '12', '4');
INSERT INTO `keldes` VALUES ('140', 'BETENUNG', '12', '3');
INSERT INTO `keldes` VALUES ('141', 'CEGOLAK', '12', '1');
INSERT INTO `keldes` VALUES ('142', 'KAYONG HULU', '12', '3');
INSERT INTO `keldes` VALUES ('143', 'KAYONG UTARA', '12', '1');
INSERT INTO `keldes` VALUES ('144', 'KAYUNG TUHE', '12', '4');
INSERT INTO `keldes` VALUES ('145', 'LEMBAH HIJAU DUA', '12', '2');
INSERT INTO `keldes` VALUES ('146', 'LEMBAH HIJAU SATU', '12', '2');
INSERT INTO `keldes` VALUES ('147', 'MENSUBANG', '12', '3');
INSERT INTO `keldes` VALUES ('148', 'NANGA TAYAP', '12', '9');
INSERT INTO `keldes` VALUES ('149', 'PANGKALAN SUKA', '12', '3');
INSERT INTO `keldes` VALUES ('150', 'PANGKALAN TELOK', '12', '5');
INSERT INTO `keldes` VALUES ('151', 'PATEH BENTENG', '12', '1');
INSERT INTO `keldes` VALUES ('152', 'SEBADAK RAYA', '12', '4');
INSERT INTO `keldes` VALUES ('153', 'SEPAKAT JAYA', '12', '3');
INSERT INTO `keldes` VALUES ('154', 'SIANTAU RAYA', '12', '5');
INSERT INTO `keldes` VALUES ('155', 'SIMPANG TIGA SEMBELANGAAN', '12', '5');
INSERT INTO `keldes` VALUES ('156', 'SUNGAI KELIK', '12', '9');
INSERT INTO `keldes` VALUES ('157', 'TAJOK KAYONG', '12', '2');
INSERT INTO `keldes` VALUES ('158', 'TANJUNG MEDAN', '12', '2');
INSERT INTO `keldes` VALUES ('159', 'KERTA BARU', '13', '1');
INSERT INTO `keldes` VALUES ('160', 'LALANG PANJANG', '13', '2');
INSERT INTO `keldes` VALUES ('161', 'MUARA GERUNGGANG', '13', '2');
INSERT INTO `keldes` VALUES ('162', 'MUARA SEMAYOK', '13', '1');
INSERT INTO `keldes` VALUES ('163', 'PEBIHINGAN', '13', '4');
INSERT INTO `keldes` VALUES ('164', 'SEMAYOK BARU', '13', '2');
INSERT INTO `keldes` VALUES ('165', 'USAHA BARU', '13', '1');
INSERT INTO `keldes` VALUES ('166', 'ALAM PAKUAN', '14', '2');
INSERT INTO `keldes` VALUES ('167', 'DEMIT', '14', '3');
INSERT INTO `keldes` VALUES ('168', 'ISTANA', '14', '6');
INSERT INTO `keldes` VALUES ('169', 'JAGO BERSATU', '14', '1');
INSERT INTO `keldes` VALUES ('170', 'MERIMBANG JAYA', '14', '3');
INSERT INTO `keldes` VALUES ('171', 'MUARA JEKAK', '14', '6');
INSERT INTO `keldes` VALUES ('172', 'PENDAMARAN INDAH', '14', '3');
INSERT INTO `keldes` VALUES ('173', 'PENJAWAAN', '14', '5');
INSERT INTO `keldes` VALUES ('174', 'PETAI PATAH', '14', '6');
INSERT INTO `keldes` VALUES ('175', 'RANDAU', '14', '3');
INSERT INTO `keldes` VALUES ('176', 'RANDAU JUNGKAL', '14', '3');
INSERT INTO `keldes` VALUES ('177', 'SANDAI KANAN', '14', '17');
INSERT INTO `keldes` VALUES ('178', 'SANDAI KIRI', '14', '6');
INSERT INTO `keldes` VALUES ('179', 'BATU DAYA', '15', '2');
INSERT INTO `keldes` VALUES ('180', 'GEMA', '15', '5');
INSERT INTO `keldes` VALUES ('181', 'KAMORA', '15', '1');
INSERT INTO `keldes` VALUES ('182', 'KAMPAR SEBOMBAN', '15', '6');
INSERT INTO `keldes` VALUES ('183', 'MEKAR RAYA', '15', '2');
INSERT INTO `keldes` VALUES ('184', 'SEMANDANG KANAN', '15', '7');
INSERT INTO `keldes` VALUES ('185', 'BALAI PINANG', '16', '8');
INSERT INTO `keldes` VALUES ('186', 'BALAI PINANG HULU', '16', '5');
INSERT INTO `keldes` VALUES ('187', 'BOTUH BOSI', '16', '5');
INSERT INTO `keldes` VALUES ('188', 'KENANGA', '16', '4');
INSERT INTO `keldes` VALUES ('189', 'KUALAN HILIR', '16', '9');
INSERT INTO `keldes` VALUES ('190', 'KUALAN HULU', '16', '6');
INSERT INTO `keldes` VALUES ('191', 'KUALAN TENGAH', '16', '8');
INSERT INTO `keldes` VALUES ('192', 'LABAI HILIR', '16', '4');
INSERT INTO `keldes` VALUES ('193', 'LEGONG', '16', '3');
INSERT INTO `keldes` VALUES ('194', 'MERAWA', '16', '8');
INSERT INTO `keldes` VALUES ('195', 'PAOH CONCONG', '16', '5');
INSERT INTO `keldes` VALUES ('196', 'SEKUCING KUALAN', '16', '3');
INSERT INTO `keldes` VALUES ('197', 'SEKUCING LABAI', '16', '8');
INSERT INTO `keldes` VALUES ('198', 'SEMANDANG HULU', '16', '3');
INSERT INTO `keldes` VALUES ('199', 'SEMANDANG KIRI', '16', '6');
INSERT INTO `keldes` VALUES ('200', 'BUKIT KELAMBING', '17', '3');
INSERT INTO `keldes` VALUES ('201', 'MUNTAI', '17', '2');
INSERT INTO `keldes` VALUES ('202', 'PANTAI KETIKAL', '17', '2');
INSERT INTO `keldes` VALUES ('203', 'SUKA MULYA', '17', '4');
INSERT INTO `keldes` VALUES ('204', 'SUKA RAJA', '17', '5');
INSERT INTO `keldes` VALUES ('205', 'SUKA SARI', '17', '3');
INSERT INTO `keldes` VALUES ('206', 'SUKAHARJA', '17', '4');
INSERT INTO `keldes` VALUES ('207', 'TANAH HITAM', '17', '2');
INSERT INTO `keldes` VALUES ('208', 'BAYUN SARI', '18', '2');
INSERT INTO `keldes` VALUES ('209', 'BENGARAS', '18', '2');
INSERT INTO `keldes` VALUES ('210', 'HARAPAN BARU', '18', '2');
INSERT INTO `keldes` VALUES ('211', 'KEPARI', '18', '2');
INSERT INTO `keldes` VALUES ('212', 'LANJUT MEKAR SARI', '18', '1');
INSERT INTO `keldes` VALUES ('213', 'MEKAR HARAPAN', '18', '2');
INSERT INTO `keldes` VALUES ('214', 'MERABU JAYA', '18', '1');
INSERT INTO `keldes` VALUES ('215', 'RANDAU LIMAT', '18', '1');
INSERT INTO `keldes` VALUES ('216', 'RIAM BUNUT', '18', '3');
INSERT INTO `keldes` VALUES ('217', 'SELANGKUT RAYA', '18', '2');
INSERT INTO `keldes` VALUES ('218', 'SEMPURNA', '18', '4');
INSERT INTO `keldes` VALUES ('219', 'SEPOTONG', '18', '3');
INSERT INTO `keldes` VALUES ('220', 'SINAR KURI', '18', '1');
INSERT INTO `keldes` VALUES ('221', 'SUKARAMAI', '18', '2');
INSERT INTO `keldes` VALUES ('222', 'SUNGAI DAKA', '18', '3');
INSERT INTO `keldes` VALUES ('223', 'TANJUNG BERINGIN', '18', '2');
INSERT INTO `keldes` VALUES ('224', 'TANJUNG MAJU', '18', '2');
INSERT INTO `keldes` VALUES ('225', 'TELUK BAYUR', '18', '3');
INSERT INTO `keldes` VALUES ('226', 'TELUK MUTIARA', '18', '2');
INSERT INTO `keldes` VALUES ('227', 'BERINGIN JAYA', '19', '2');
INSERT INTO `keldes` VALUES ('228', 'JAIRAN JAYA', '19', '2');
INSERT INTO `keldes` VALUES ('229', 'KARYA MUKTI', '19', '2');
INSERT INTO `keldes` VALUES ('230', 'KEPULUK', '19', '2');
INSERT INTO `keldes` VALUES ('231', 'MAKMUR ABADI', '19', '2');
INSERT INTO `keldes` VALUES ('232', 'MEKAR JAYA', '19', '3');
INSERT INTO `keldes` VALUES ('233', 'PIANSAK', '19', '3');
INSERT INTO `keldes` VALUES ('234', 'SUKA MULYA', '19', '2');
INSERT INTO `keldes` VALUES ('235', 'SUNGAI MELAYU', '19', '8');
INSERT INTO `keldes` VALUES ('236', 'SUNGAI MELAYU BARU', '19', '4');
INSERT INTO `keldes` VALUES ('237', 'SUNGAI MELAYU JAYA', '19', '2');
INSERT INTO `keldes` VALUES ('238', 'AUR GADING', '20', '2');
INSERT INTO `keldes` VALUES ('239', 'BATU BERANSAK', '20', '2');
INSERT INTO `keldes` VALUES ('240', 'BATU TAJAM', '20', '2');
INSERT INTO `keldes` VALUES ('241', 'BELABAN TUJUH', '20', '2');
INSERT INTO `keldes` VALUES ('242', 'BERINGIN RAYO', '20', '3');
INSERT INTO `keldes` VALUES ('243', 'JELAYAN', '20', '1');
INSERT INTO `keldes` VALUES ('244', 'JUNGKAL', '20', '3');
INSERT INTO `keldes` VALUES ('245', 'KALIMAS BARU', '20', '4');
INSERT INTO `keldes` VALUES ('246', 'MAHAWA', '20', '4');
INSERT INTO `keldes` VALUES ('247', 'NANGA KELAMPAI', '20', '2');
INSERT INTO `keldes` VALUES ('248', 'NATAI PANJANG', '20', '3');
INSERT INTO `keldes` VALUES ('249', 'PAMUATAN JAYA', '20', '3');
INSERT INTO `keldes` VALUES ('250', 'PENGATAPAN RAYA', '20', '2');
INSERT INTO `keldes` VALUES ('251', 'PETEBANG JAYA', '20', '2');
INSERT INTO `keldes` VALUES ('252', 'SEGAR WANGI', '20', '4');
INSERT INTO `keldes` VALUES ('253', 'SENGKAHARAK', '20', '2');
INSERT INTO `keldes` VALUES ('254', 'SEPAUHAN RAYA', '20', '2');
INSERT INTO `keldes` VALUES ('255', 'SEPURING INDAH', '20', '1');
INSERT INTO `keldes` VALUES ('256', 'SERENGKAH', '20', '2');
INSERT INTO `keldes` VALUES ('257', 'SERENGKAH KANAN', '20', '1');
INSERT INTO `keldes` VALUES ('258', 'SUKA DAMAI', '20', '1');
INSERT INTO `keldes` VALUES ('259', 'TANJUNG BEULANG', '20', '1');
INSERT INTO `keldes` VALUES ('260', 'TANJUNG MALOI', '20', '1');
INSERT INTO `keldes` VALUES ('261', 'TITI BARU', '20', '5');
INSERT INTO `keldes` VALUES ('262', 'TUMBANG TITI', '20', '6');

-- ----------------------------
-- Table structure for `pemilih`
-- ----------------------------
DROP TABLE IF EXISTS `pemilih`;
CREATE TABLE `pemilih` (
  `nik` bigint(16) NOT NULL,
  `nama_pemilih` varchar(40) NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `id_keldes` int(11) NOT NULL,
  `id_tps` int(11) NOT NULL,
  `kontak` varchar(13) NOT NULL,
  `status` varchar(20) NOT NULL,
  `tgl` date NOT NULL DEFAULT current_timestamp(),
  `jam` time NOT NULL DEFAULT current_timestamp(),
  `operator` varchar(20) NOT NULL,
  PRIMARY KEY (`nik`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of pemilih
-- ----------------------------
INSERT INTO `pemilih` VALUES ('6104171300000001', 'ganz', '3', '23', '0', '001', 'pengurus partai', '2020-11-21', '18:14:25', 'lanjutkan');
INSERT INTO `pemilih` VALUES ('6104171300000002', 'herman', '2', '15', '0', '009', 'pengurus partai', '2020-11-21', '18:14:47', 'lanjutkan');
INSERT INTO `pemilih` VALUES ('6104171300000003', 'nusa', '3', '23', '1', '002', 'saksi', '2020-11-21', '18:27:12', 'dapil1');

-- ----------------------------
-- Table structure for `suara`
-- ----------------------------
DROP TABLE IF EXISTS `suara`;
CREATE TABLE `suara` (
  `id_suara` int(11) NOT NULL AUTO_INCREMENT,
  `id_adm` varchar(16) NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `id_keldes` int(11) NOT NULL,
  `id_tps` int(11) NOT NULL,
  `paslon1` int(11) NOT NULL,
  `paslon2` int(11) NOT NULL,
  `paslon3` int(11) NOT NULL,
  `paslon4` int(11) NOT NULL,
  `tidaksah` int(11) NOT NULL,
  `total_dptb` int(11) NOT NULL,
  `tanggal` date NOT NULL DEFAULT current_timestamp(),
  `waktu` time NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_suara`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of suara
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_adm`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_adm`;
CREATE TABLE `tbl_adm` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `id_pengguna` varchar(20) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` varchar(12) NOT NULL,
  `level_dapil` int(11) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_adm
-- ----------------------------
INSERT INTO `tbl_adm` VALUES ('1', 'development', 'ganz1234', '6e76314f7a943d8027320110f158d605', 'superadmin', '0');
INSERT INTO `tbl_adm` VALUES ('2', '1001', 'lanjutkan', '492e23ae586d7413ad881679d1f20268', 'superadmin', '0');
INSERT INTO `tbl_adm` VALUES ('68', '6104171300000001', 'dapil1', '58451fa69d7f1e18f7f8447b4d1d5fea', 'operasional', '1');
INSERT INTO `tbl_adm` VALUES ('70', '6104171300000003', 'nusa', '61d986096dadf2607e5cb618ac1a21ad', 'tps', '1');
INSERT INTO `tbl_adm` VALUES ('71', '6104171300000002', 'dapil6', '1d1cdb59b049d8c72c64895def8656d5', 'operasional', '6');

-- ----------------------------
-- Table structure for `tbl_user`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `ids` int(11) NOT NULL AUTO_INCREMENT,
  `id_pengguna` varchar(20) NOT NULL,
  `level_dapil` int(20) NOT NULL,
  PRIMARY KEY (`ids`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_user
-- ----------------------------
INSERT INTO `tbl_user` VALUES ('43', '6104171300000001', '1');
INSERT INTO `tbl_user` VALUES ('44', '6104171300000002', '6');

-- ----------------------------
-- Table structure for `tps`
-- ----------------------------
DROP TABLE IF EXISTS `tps`;
CREATE TABLE `tps` (
  `id_tps` int(11) NOT NULL AUTO_INCREMENT,
  `nama_tps` varchar(40) NOT NULL,
  PRIMARY KEY (`id_tps`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tps
-- ----------------------------
INSERT INTO `tps` VALUES ('1', 'TPS 001');
INSERT INTO `tps` VALUES ('2', 'TPS 002');
INSERT INTO `tps` VALUES ('3', 'TPS 003');
INSERT INTO `tps` VALUES ('4', 'TPS 004');
INSERT INTO `tps` VALUES ('5', 'TPS 005');
INSERT INTO `tps` VALUES ('6', 'TPS 006');
INSERT INTO `tps` VALUES ('7', 'TPS 007');
INSERT INTO `tps` VALUES ('8', 'TPS 008');
INSERT INTO `tps` VALUES ('9', 'TPS 009');
INSERT INTO `tps` VALUES ('10', 'TPS 010');
INSERT INTO `tps` VALUES ('11', 'TPS 011');
INSERT INTO `tps` VALUES ('12', 'TPS 012');
INSERT INTO `tps` VALUES ('13', 'TPS 013');
INSERT INTO `tps` VALUES ('14', 'TPS 014');
INSERT INTO `tps` VALUES ('15', 'TPS 015');
INSERT INTO `tps` VALUES ('16', 'TPS 016');
INSERT INTO `tps` VALUES ('17', 'TPS 017');
INSERT INTO `tps` VALUES ('18', 'TPS 018');
INSERT INTO `tps` VALUES ('19', 'TPS 019');
INSERT INTO `tps` VALUES ('20', 'TPS 020');
INSERT INTO `tps` VALUES ('21', 'TPS 021');
INSERT INTO `tps` VALUES ('22', 'TPS 022');
INSERT INTO `tps` VALUES ('23', 'TPS 023');
INSERT INTO `tps` VALUES ('24', 'TPS 024');
INSERT INTO `tps` VALUES ('25', 'TPS 025');
INSERT INTO `tps` VALUES ('26', 'TPS 026');
INSERT INTO `tps` VALUES ('27', 'TPS 027');
INSERT INTO `tps` VALUES ('28', 'TPS 028');
INSERT INTO `tps` VALUES ('29', 'TPS 029');
INSERT INTO `tps` VALUES ('30', 'TPS 030');
INSERT INTO `tps` VALUES ('31', 'TPS 031');
INSERT INTO `tps` VALUES ('32', 'TPS 032');
INSERT INTO `tps` VALUES ('33', 'TPS 033');
INSERT INTO `tps` VALUES ('34', 'TPS 034');
INSERT INTO `tps` VALUES ('35', 'TPS 035');
INSERT INTO `tps` VALUES ('36', 'TPS 036');
INSERT INTO `tps` VALUES ('37', 'TPS 037');
INSERT INTO `tps` VALUES ('38', 'TPS 038');
INSERT INTO `tps` VALUES ('39', 'TPS 039');
INSERT INTO `tps` VALUES ('40', 'TPS 040');
